'''

This code contains the visualization tool developed by I. del Moral-Castro (IAC, ignaciodelmoralcastro@gmail.com). The basic functionality of this interactive tool is to visualise IFU data (e.g. CALIFA, MANGA, SAMI, MUSE ...)

'''
########################################################################
#     importing libraries                                              #
########################################################################

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
from astropy.io import fits
import matplotlib.gridspec as gridspec

gal = 'NGC2253.fits.gz' # input data

########################################################################
#     opening data and defining wavelength range                       #
########################################################################

hdu_list = fits.open(gal) 

try:
  image_data = hdu_list[0].data
  image_data[image_data == 0] = np.nan # convert 0 to nan
  naxis1, naxis2, naxis3, crval3, cdelt3 = hdu_list[0].header['NAXIS1'], hdu_list[0].header['NAXIS2'], hdu_list[0].header['NAXIS3'], hdu_list[0].header['CRVAL3'], hdu_list[0].header['CDELT3']

except:
  image_data = hdu_list[1].data
  image_data[image_data == 0] = np.nan # convert 0 to nan
  naxis1, naxis2, naxis3, crval3, cdelt3 = hdu_list[1].header['NAXIS1'], hdu_list[1].header['NAXIS2'], hdu_list[1].header['NAXIS3'], hdu_list[1].header['CRVAL3'], hdu_list[1].header['CD3_3']

lamb = np.arange(crval3, crval3+cdelt3*naxis3, cdelt3) #wavelength

########################################################################
#                plotting figures                                      #
########################################################################

fig = plt.figure(figsize=(16,9))
spec2 = gridspec.GridSpec(ncols=3, nrows=3)
ax1, ax2 = fig.add_subplot(spec2[1:3, 0:2]), fig.add_subplot(spec2[0, 0:3])
spec2.update(hspace=0.45)

#### initial values

w0, slide, cmap0, max_value = int(naxis3/2), int(naxis3/2) , 'jet', np.nanmax(image_data)

#### figure 1: imshow

ax1.set_title(str(lamb[slide])+' $\AA$')
x0, y0 = int(naxis1/2), int(naxis2/2)
#norm = simple_norm(image_data[w0], 'linear')

l = ax1.imshow(image_data[w0], origin='lower', vmin=0, vmax=max_value,cmap=cmap0, norm=None)

l4, = ax1.plot(x0, y0, '+', color='black')

ax1.set_xlim(0,naxis1), ax1.set_ylim(0,naxis2) # defining axis limits
ax1.set_xlabel('Spaxel'), ax1.set_ylabel('Spaxel') # defining axis names

cax = fig.add_axes([0.52, 0.12, 0.01, 0.45])
cbar = fig.colorbar(l, cax=cax,orientation="vertical")
cbar.set_label('Flux')

#### figure 2: spectra

ax2.set_title('Spaxel'+' '+str(x0)+','+str(y0))
l2, = ax2.plot(lamb[:], image_data[:,x0, y0], label='emision')
l3, = ax2.plot([lamb[w0],lamb[w0]],[-10,10]) # interactive vertical line

ax2.set_xlim(min(lamb) - 100, max(lamb) + 100), ax2.set_ylim(np.nanmin(image_data[:,x0, y0])-0.1,np.nanmax(image_data[:,x0, y0])+0.1) # defining axis limits
ax2.set_xlabel(r'$\lambda$'), ax2.set_ylabel('Flux') # defining axis names

#### defining interactive Slide 

axfreq = plt.axes([0.1442, 0.98, 0.738, 0.01])
sslide = Slider(axfreq, '', 0, naxis3-2, valinit=w0, valfmt="%i")

slide_values = np.arange(0, naxis3-2, len(lamb)/8) 
lamb_values = np.round(lamb[slide_values],1)

axfreq.set_xticks(slide_values, minor = False)
axfreq.set_xticklabels(lamb_values)

def update(val):
    global slide
    slide = sslide.val
    l.set_data(image_data[int(slide)])
    l3.set_data([lamb[int(slide)], lamb[int(slide)]],[-10,10])
    ax1.set_title(str(lamb[int(slide)])+' $\AA$')
    fig.canvas.draw_idle()
sslide.on_changed(update)

##### defining interactive button for cmaps

rax = plt.axes([0.65, 0.25, 0.15, 0.2])
radio = RadioButtons(rax, ('jet', 'viridis', 'gnuplot', 'gnuplot2', 'cubehelix', 'nipy_spectral', 'RdBu'), active=0)

def colorfunc(label):
    l.set_cmap(label)
    plt.draw()
radio.on_clicked(colorfunc)

##### defining interactive limits for cmaps

c_max, c_min = max_value, 0
ax_cmax, ax_cmin = plt.axes([0.65, 0.15, 0.15, 0.03]), plt.axes([0.65, 0.1, 0.15, 0.03])
s_cmax, s_cmin = Slider(ax_cmax, 'max', 0, c_max, valinit=c_max), Slider(ax_cmin, 'min', 0, c_max, valinit=c_min)

def update(val, s=None):
    _cmin, _cmax = s_cmin.val, s_cmax.val
    l.set_clim([_cmin, _cmax])
    plt.draw()

s_cmin.on_changed(update), s_cmax.on_changed(update)

### defining interactive click

def onclick(event):
    # Check if the click was in ax1
    if event.inaxes in [ax1]:
        if int(event.xdata) < naxis1 and int(event.ydata) < naxis2:
            indexx, indexy = int(event.xdata), int(event.ydata)
            l4.set_data(indexx, indexy)
            l2.set_data(lamb[:], image_data[:, indexy,indexx])
            ax2.set_title('Spaxel'+' '+str(indexx)+','+str(indexy))
            ax2.set_ylim(np.nanmin(image_data[:,indexy,indexx])-0.1,np.nanmax(image_data[:,indexy,indexx])+0.1)
            plt.draw()

cid = fig.canvas.mpl_connect('button_press_event', onclick)    

### reset button

resetax = plt.axes([0.85, 0.1, 0.1, 0.1])
button = Button(resetax, 'Reset', color='lightgoldenrodyellow', hovercolor='0.975')

def reset(event):
    """Reset the interactive plots to inital values: slide and vmin and vmax."""
    sslide.reset(), s_cmin.reset(), s_cmax.reset()

button.on_clicked(reset)

plt.show()
